class AppointmentNotFound(Exception):
    def __init__(self) -> None:
        super().__init__("Appointment was not found")


class OverlappingAppointment(Exception):
    def __init__(self) -> None:
        super().__init__("New appointment is overlapping existing appointments")
