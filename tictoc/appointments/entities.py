from datetime import datetime, timedelta

from pony.orm import PrimaryKey, Required

from tictoc.common.database import BaseEntity


class Appointment(BaseEntity):
    _table_ = "appointments"

    appointment_id = PrimaryKey(int, auto=True)
    description = Required(str, max_len=100)
    start_date = Required(datetime)
    created_at = Required(datetime, default=datetime.now())
    service = Required("Service")

    @property
    def end_date(self) -> datetime:
        end_date = self.start_date + timedelta(minutes=self.service.duration_in_minutes)

        return end_date
