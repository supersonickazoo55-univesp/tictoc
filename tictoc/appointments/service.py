from datetime import datetime, timedelta
from functools import lru_cache
from typing import Any, Dict

from pony.orm.serialization import to_dict

from tictoc.appointments.entities import Appointment
from tictoc.common.configuration import Configuration, configuration_factory
from tictoc.common.dto import ListingResponse
from tictoc.services.entities import Service
from tictoc.services.service import service_factory as service_service_factory

from .exceptions import OverlappingAppointment
from .repository import AppointmentRepository, repository_factory


class AppointmentService:

    def __init__(
        self, repository: AppointmentRepository, configuration: Configuration
    ) -> None:
        self._repository = repository
        self._configuration = configuration
        self._service_service = service_service_factory()

    def create_appointment(
        self, description: str, start_date: datetime, service_id: int
    ) -> Appointment:
        service = self._service_service.get_service_by_service_id(service_id)
        end_date = start_date + timedelta(minutes=service.duration_in_minutes)

        if self._repository.is_overlapping_appointments(start_date, end_date):
            raise OverlappingAppointment

        new_appointment = self._repository.create_appointment(
            description, start_date, service.service_id
        )
        return new_appointment

    def get_appointment_by_appointment_id(self, appointment_id: int) -> Appointment:
        return self._repository.get_appointment_by_appointment_id(appointment_id)

    def list_appointments(
        self,
        page: int,
        limit: int,
        description: str = None,
        start_date: datetime = None,
        end_date: datetime = None,
    ) -> ListingResponse[Dict[str, Any]]:
        data = self._repository.list_appointments(
            page,
            limit,
            description=description,
            start_date=start_date,
            end_date=end_date,
        )
        data.results = to_dict(data.results)
        return data

    def delete_appointment(self, appointment_id: int) -> None:
        appointment = self._repository.delete_appointment_by_appointment_id(
            appointment_id
        )

    def update_appointment_by_appointment_id(
        self,
        appointment_id: int,
        description: str = None,
        start_date: str = None,
        service: Service = None,
    ) -> Appointment:
        updated_appointment = self._repository.update_appointment(
            appointment_id,
            description=description,
            start_date=start_date,
            service=service,
        )
        return updated_appointment


@lru_cache
def service_factory() -> AppointmentService:
    configuration = configuration_factory()
    repository = repository_factory()
    return AppointmentService(repository, configuration)
