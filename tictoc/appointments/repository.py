from datetime import datetime
from functools import lru_cache
from math import ceil
from typing import List

from pony.orm import commit, db_session, rollback, select

from tictoc.common.database import ListingResults, database_factory
from tictoc.services.entities import Service

from .entities import Appointment
from .exceptions import AppointmentNotFound


class AppointmentRepository:

    def __init__(self) -> None:
        self._database = database_factory()

    @db_session
    def create_appointment(
        self, description: str, start_date: datetime, service: Service
    ) -> Appointment:
        new_appointment = Appointment(
            description=description, start_date=start_date, service=service
        )
        commit()
        return new_appointment

    @db_session
    def get_appointment_by_appointment_id(self, appointment_id: int) -> Appointment:
        appointment = (
            select(appointment for appointment in Appointment)
            .filter(lambda appointment: appointment.appointment_id == appointment_id)
            .prefetch(Appointment.service)
        ).get()

        if not appointment:
            raise AppointmentNotFound

        return appointment

    @db_session
    def list_appointments(
        self,
        page: int,
        limit: int,
        start_date: datetime = None,
        end_date: datetime = None,
        description: str = None,
    ) -> ListingResults[Appointment]:
        appointments = select(appointment for appointment in Appointment).prefetch(
            Appointment.service
        )

        if start_date:
            appointments = appointments.filter(
                lambda appointment: appointment.start_date >= start_date
            )

        if end_date:
            appointments = appointments.filter(
                lambda appointment: appointment.start_date <= end_date
            )

        if description:
            appointments = appointments.filter(
                lambda appointment: description.lower()
                in appointment.description.lower()
            )

        appointments = appointments.order_by(Appointment.appointment_id)
        results = appointments.page(page, limit)
        count = appointments.count()

        return ListingResults(
            results=results[:],
            count=count,
            last_page=int(ceil(count / limit)),
            page=page,
        )

    @db_session
    def update_appointment(
        self,
        appointment_id: int,
        description: str = None,
        start_date: datetime = None,
        service: Service = None,
    ) -> Appointment:
        appointment = self.get_appointment_by_appointment_id(appointment_id)
        appointment.set(description=description, start_date=start_date, service=service)
        commit()
        return appointment

    @db_session
    def delete_appointment_by_appointment_id(self, appointment_id: int) -> None:
        appointment = self.get_appointment_by_appointment_id(appointment_id)
        appointment.delete()
        commit()

    @db_session
    def is_overlapping_appointments(
        self,
        start_date: datetime,  # pylint: disable = unused-argument
        end_date: datetime,  # pylint: disable = unused-argument
    ) -> List[Appointment]:
        overlapping_appointments = self._database.select(
            """SELECT a.*
            FROM appointments a
            JOIN services s ON s.service_id = a.service
            WHERE (
                LEAST(
                    DATE_ADD(
                        a.start_date,
                        INTERVAL s.duration_in_minutes MINUTE
                    ),
                $end_date)
                - GREATEST(
                    a.start_date, 
                    $start_date
                )
            ) > 0"""
        )
        return bool(overlapping_appointments[:])


@lru_cache
def repository_factory() -> AppointmentRepository:
    return AppointmentRepository()
