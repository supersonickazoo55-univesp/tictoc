from dataclasses import dataclass
from typing import Generic, List, TypeVar

T = TypeVar("T")


@dataclass
class ListingResults(Generic[T]):
    results: List[T]
    page: int
    count: int
    last_page: int

    def __post_init__(self) -> None:
        self.last_page = self.last_page or 1
