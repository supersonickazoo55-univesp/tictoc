from .database import database_factory

database = database_factory()

BaseEntity = database.Entity
