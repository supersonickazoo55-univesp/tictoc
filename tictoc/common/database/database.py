from functools import lru_cache

from pony.orm import Database

from tictoc.common.configuration import Configuration, configuration_factory


class ProviderNotFound(Exception):
    def __init__(self) -> None:
        super().__init__("Database provider not found")


class BaseDatabase(Database):

    def __init__(self, configuration: Configuration) -> None:
        super().__init__()
        self._configuration = configuration
        self._bind_database()

    def start_database(self) -> None:
        self.generate_mapping(create_tables=True, check_tables=True)

    def _bind_database(self) -> None:
        raise NotImplementedError("please, implement method _bind_database")


class MySQLDatabase(BaseDatabase):
    def _bind_database(self) -> None:
        self.bind(
            provider=self._configuration.configuration.database.provider,
            host=self._configuration.configuration.database.host,
            user=self._configuration.configuration.database.user,
            passwd=self._configuration.configuration.database.password,
            db=self._configuration.configuration.database.database,
        )


class SQLiteDatabase(BaseDatabase):
    def _bind_database(self) -> None:
        self.bind(
            provider=self._configuration.configuration.database.provider,
            filename=self._configuration.configuration.database.filename,
        )


@lru_cache
def database_factory() -> Database:
    configuration = configuration_factory()
    provider = configuration.configuration.database.provider
    providers = {"mysql": MySQLDatabase, "sqlite": SQLiteDatabase}

    if provider not in providers:
        raise ProviderNotFound

    return providers[provider](configuration)
