from typing import Any, Generic, List, TypeVar

from pony.orm.serialization import to_dict
from pydantic import BaseModel as Base
from pydantic import model_validator

from tictoc.common.database import BaseEntity

ListItem = TypeVar("ListItem")


class BaseModel(Base):
    pass


class ListingResponse(Base, Generic[ListItem]):
    data: List[ListItem]
    page: int
    last_page: int
    count: int
