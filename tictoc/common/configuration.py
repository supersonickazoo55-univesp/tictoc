from dataclasses import dataclass
from functools import lru_cache
from os import environ

import tomli


@dataclass
class ConfigurationDatabase:
    user: str
    password: str
    host: str
    provider: str
    database: str
    filename: str


@dataclass
class ConfigurationAuthentication:
    secret: str


@dataclass
class ConfigurationBase:
    database: ConfigurationDatabase
    authentication: ConfigurationAuthentication

    def __post_init__(self):
        self.database = ConfigurationDatabase(**self.database)
        self.authentication = ConfigurationAuthentication(**self.authentication)


class Configuration:
    def __init__(self) -> None:
        self.filename = environ.get("TICTOC_CONFIGURATION_FILE")
        self.configuration = self._initialize(self.filename)

    def _initialize(self, filename: str) -> ConfigurationBase:
        with open(filename, "rb") as input_file:
            return ConfigurationBase(**tomli.load(input_file))


@lru_cache
def configuration_factory() -> Configuration:
    return Configuration()
