from datetime import datetime
from typing import Annotated, Any, Dict

from fastapi import APIRouter, Depends, Form, Request
from fastapi.responses import HTMLResponse, RedirectResponse
from fastapi.templating import Jinja2Templates

from tictoc.appointments.service import service_factory as appointment_service_factory
from tictoc.services.service import service_factory as service_service_factory
from tictoc.views.authentication import get_current_user

router = APIRouter(prefix="/appointments")
templates = Jinja2Templates(directory="tictoc/templates")

appointment_service = appointment_service_factory()
service_service = service_service_factory()


@router.get("/")
def list_appointments(
    request: Request,
    current_user: Annotated[Dict[str, Any], Depends(get_current_user)],
    page: int = 1,
    limit: int = 20,
    description: str = None,
    start_date: datetime = None,
    end_date: datetime = None,
) -> HTMLResponse:
    data = appointment_service.list_appointments(
        page=page,
        limit=limit,
        description=description,
        start_date=start_date,
        end_date=end_date,
    )
    return templates.TemplateResponse(
        request=request, name="appointments.html", context={"data": data}
    )


@router.get("/update/{appointment_id}")
def update_appointment(
    request: Request,
    current_user: Annotated[Dict[str, Any], Depends(get_current_user)],
    appointment_id: int,
    error_message: str = None,
) -> HTMLResponse:
    services = service_service.list_services(1, 1000)
    appointment = appointment_service.get_appointment_by_appointment_id(appointment_id)
    return templates.TemplateResponse(
        request=request,
        name="appointment_update.html",
        context={
            "services": services,
            "appointment_id": appointment_id,
            "description": appointment.description,
            "start_date": appointment.start_date,
            "service": appointment.service.service_id,
            "error_message": error_message,
        },
    )


@router.post("/update/{appointment_id}/api")
def update_appointment_api(
    request: Request,
    current_user: Annotated[Dict[str, Any], Depends(get_current_user)],
    appointment_id: int,
    description: Annotated[str, Form()],
    start_date: Annotated[datetime, Form()],
    service: Annotated[int, Form()],
) -> HTMLResponse:
    error_message = None

    try:
        appointment_service.update_appointment_by_appointment_id(
            appointment_id=appointment_id,
            description=description,
            start_date=start_date,
            service=service,
        )
    except Exception as exc:
        error_message = str(exc)
        update_appointment(request, current_user, appointment_id, error_message)

    return list_appointments(request, current_user)


@router.get("/delete/{appointment_id}")
def delete_appointment(
    request: Request,
    current_user: Annotated[Dict[str, Any], Depends(get_current_user)],
    appointment_id: int,
):
    appointment_service.delete_appointment(appointment_id)
    return RedirectResponse(router.url_path_for("list_appointments"))


@router.get("/create")
def create_appointment(
    request: Request,
    current_user: Annotated[Dict[str, Any], Depends(get_current_user)],
    error_message: str = None,
) -> HTMLResponse:
    services = service_service.list_services(1, 1000)
    return templates.TemplateResponse(
        request=request,
        name="appointment_create.html",
        context={
            "start_date": datetime.now(),
            "services": services,
            "error_message": error_message,
        },
    )


@router.post("/create/api")
def create_appointment_api(
    request: Request,
    current_user: Annotated[Dict[str, Any], Depends(get_current_user)],
    description: Annotated[str, Form()],
    start_date: Annotated[datetime, Form()],
    service: Annotated[int, Form()],
) -> HTMLResponse:
    try:
        appointment_service.create_appointment(description, start_date, service)
        return list_appointments(request, current_user)
    except Exception as exc:
        return create_appointment(request, current_user, error_message=str(exc))
