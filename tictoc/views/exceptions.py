class ApiError(Exception):
    def __init__(self, message: str, status_code: int) -> None:
        super().__init__(message)
        self.message = message
        self.status_code = status_code


class Unauthorized(ApiError):
    def __init__(self) -> None:
        super().__init__("User is unauthorized to access resource", 403)
