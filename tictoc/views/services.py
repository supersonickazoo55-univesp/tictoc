from datetime import datetime
from typing import Annotated, Any, Dict

from fastapi import APIRouter, Depends, Form, Request
from fastapi.responses import HTMLResponse, RedirectResponse
from fastapi.templating import Jinja2Templates

from tictoc.services.service import service_factory
from tictoc.views.authentication import get_current_user

router = APIRouter(prefix="/services")
templates = Jinja2Templates(directory="tictoc/templates")

service_service = service_factory()


@router.get("/")
def list_services(
    request: Request,
    current_user: Annotated[Dict[str, Any], Depends(get_current_user)],
    page: int = 1,
    limit: int = 20,
    name: str = None,
    description: str = None,
) -> HTMLResponse:
    data = service_service.list_services(
        page=page, limit=limit, name=name, description=description
    )
    return templates.TemplateResponse(
        request=request, name="services.html", context={"data": data}
    )


@router.get("/update/{service_id}")
def update_service(
    request: Request,
    current_user: Annotated[Dict[str, Any], Depends(get_current_user)],
    service_id: int,
    error_message: str = None,
) -> HTMLResponse:
    service = service_service.get_service_by_service_id(service_id)
    return templates.TemplateResponse(
        request=request,
        name="service_update.html",
        context={
            "service_id": service_id,
            "name": service.name,
            "description": service.description,
            "price": service.price,
            "duration_in_minutes": service.duration_in_minutes,
            "error_message": error_message,
        },
    )


@router.post("/update/{service_id}/api")
def update_service_api(
    request: Request,
    current_user: Annotated[Dict[str, Any], Depends(get_current_user)],
    service_id: int,
    name: Annotated[str, Form()],
    description: Annotated[str, Form()],
    price: Annotated[int, Form()],
    duration_in_minutes: Annotated[int, Form()],
) -> HTMLResponse:
    error_message = None

    try:
        service_service.update_service_by_service_id(
            service_id,
            name=name,
            description=description,
            price=price,
            duration_in_minutes=duration_in_minutes,
        )
    except Exception as exc:
        error_message = str(exc)
        update_service(request, current_user, service_id, error_message)

    return list_services(request, current_user)


@router.get("/delete/{service_id}")
def delete_service(
    request: Request,
    current_user: Annotated[Dict[str, Any], Depends(get_current_user)],
    service_id: int,
) -> HTMLResponse:
    service_service.delete_service(service_id)
    return RedirectResponse(router.url_path_for("list_services"))


@router.get("/create")
def create_service(
    request: Request,
    current_user: Annotated[Dict[str, Any], Depends(get_current_user)],
    error_message: str = None,
) -> HTMLResponse:
    return templates.TemplateResponse(
        request=request,
        name="service_create.html",
        context={
            "error_message": error_message,
        },
    )


@router.post("/create/api")
def create_service_api(
    request: Request,
    current_user: Annotated[Dict[str, Any], Depends(get_current_user)],
    name: Annotated[str, Form()],
    description: Annotated[str, Form()],
    price: Annotated[int, Form()],
    duration_in_minutes: Annotated[int, Form()],
) -> HTMLResponse:
    try:
        service_service.create_service(
            name=name,
            description=description,
            price=price,
            duration_in_minutes=duration_in_minutes,
        )
        return list_services(request, current_user)
    except Exception as exc:
        return create_service(request, current_user, error_message=str(exc))
