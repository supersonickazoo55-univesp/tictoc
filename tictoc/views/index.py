from fastapi import APIRouter
from fastapi.requests import Request
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates

from tictoc.services.service import service_factory

router = APIRouter()
templates = Jinja2Templates(directory="tictoc/templates")


@router.get("/")
def home(request: Request) -> HTMLResponse:
    service_service = service_factory()
    data = service_service.list_services(1, 1000)
    return templates.TemplateResponse(
        request=request, name="index.html", context={"data": data}
    )
