from datetime import datetime
from typing import Annotated, Any, Dict

from fastapi import APIRouter, Depends, Request
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates

from tictoc.appointments.service import service_factory
from tictoc.views.authentication import get_current_user

router = APIRouter(prefix="/dashboard")
templates = Jinja2Templates(directory="tictoc/templates")
appointment_service = service_factory()


@router.get("/")
def dashboard(
    request: Request,
    current_user: Annotated[Dict[str, Any], Depends(get_current_user)],
    page: int = 1,
) -> HTMLResponse:
    data = appointment_service.list_appointments(page, 10, start_date=datetime.now())
    return templates.TemplateResponse(
        request=request,
        name="dashboard.html",
        context={"data": data},
    )
