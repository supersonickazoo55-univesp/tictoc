from typing import Annotated, Any, Dict, Union

from fastapi import APIRouter, Form, Request
from fastapi.responses import HTMLResponse, RedirectResponse
from fastapi.templating import Jinja2Templates

from tictoc.authentication.dto import LoginRequest
from tictoc.authentication.exceptions import InvalidPassword, UserNotFound
from tictoc.authentication.service import service_factory
from tictoc.authentication.token import token_factory
from tictoc.common.exceptions import ValidationError

from .exceptions import Unauthorized

authentication_service = service_factory()
token_manager = token_factory()
router = APIRouter(prefix="/authentication")
templates = Jinja2Templates(directory="tictoc/templates")


def get_current_user(request: Request) -> Dict[str, Any]:
    token = request.cookies.get("token")

    if not token or not token_manager.is_token_valid(token):
        raise Unauthorized

    payload = token_manager.decode_token(token)
    current_user = authentication_service.get_user_by_user_id(payload["sub"])
    return current_user


@router.get("/login")
def login(
    request: Request,
    username: str = None,
    password: str = None,
    error_message: str = None,
) -> HTMLResponse:
    return templates.TemplateResponse(
        request=request,
        name="login.html",
        context={
            "username": username,
            "password": password,
            "error_message": error_message,
        },
    )


@router.post("/user/login", response_model=None)
def login_user(
    request: Request,
    username: Annotated[str, Form()],
    password: Annotated[str, Form()],
) -> Union[HTMLResponse, RedirectResponse]:

    try:
        validated_request = LoginRequest(username=username, password=password)
        authentication_service.verify_user(
            validated_request.username, validated_request.password
        )
    except (UserNotFound, InvalidPassword, ValidationError) as exc:
        return login(
            request,
            username=username,
            password=password,
            error_message=str(exc),
        )

    user = authentication_service.get_user_by_username(validated_request.username)

    token = token_manager.create_token(user.user_id)

    response = RedirectResponse("/dashboard", status_code=303)
    response.set_cookie(key="token", value=token, httponly=True)
    return response
