class ServiceNotFound(Exception):
    def __init__(self) -> None:
        super().__init__("Service was not found")
