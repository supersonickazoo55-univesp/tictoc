from datetime import datetime
from decimal import Decimal

from pony.orm import Optional, PrimaryKey, Required, Set

from tictoc.common.database import BaseEntity


class Service(BaseEntity):
    _table_ = "services"

    service_id = PrimaryKey(int, auto=True)
    name = Required(str, max_len=100, unique=True, py_check=lambda x: x.lower() == x)
    description = Optional(str, max_len=100, nullable=True)
    duration_in_minutes = Required(int, min=0)
    price = Required(Decimal, scale=2, precision=10, min=0)
    created_at = Required(datetime, default=datetime.now())

    appointments = Set("Appointment")
