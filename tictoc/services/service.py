from functools import lru_cache
from typing import Any, Dict

from pony.orm.serialization import to_dict

from tictoc.common.configuration import Configuration, configuration_factory
from tictoc.common.dto import ListingResponse

from .entities import Service
from .repository import ServiceRepository, repository_factory


class ServiceService:

    def __init__(
        self, repository: ServiceRepository, configuration: Configuration
    ) -> None:
        self._repository = repository
        self._configuration = configuration

    def create_service(
        self, name: str, duration_in_minutes: int, price: float, description: str = None
    ) -> Service:
        return self._repository.create_service(
            name=name,
            description=description,
            duration_in_minutes=duration_in_minutes,
            price=price,
        )

    def get_service_by_service_id(self, service_id: int) -> Service:
        return self._repository.get_service_by_service_id(service_id)

    def list_services(
        self, page: int, limit: int, name: str = None, description: str = None
    ) -> ListingResponse[Dict[str, Any]]:
        data = self._repository.list_services(
            page=page, limit=limit, name=name, description=description
        )
        data.results = to_dict(data.results)
        return data

    def delete_service(self, service_id: int) -> None:
        service = self._repository.delete_service_by_service_id(service_id)

    def update_service_by_service_id(
        self,
        service_id: int,
        name: str = None,
        description: str = None,
        duration_in_minutes: int = None,
        price: float = None,
    ) -> Service:
        return self._repository.update_service_by_service_id(
            service_id=service_id,
            name=name,
            description=description,
            duration_in_minutes=duration_in_minutes,
            price=price,
        )


@lru_cache
def service_factory() -> ServiceService:
    configuration = configuration_factory()
    repository = repository_factory()
    return ServiceService(repository, configuration)
