from functools import lru_cache
from math import ceil

from pony.orm import commit, db_session, select

from tictoc.common.database import ListingResults, database_factory

from .entities import Service
from .exceptions import ServiceNotFound


class ServiceRepository:

    def __init__(self) -> None:
        self._database = database_factory

    @db_session
    def create_service(
        self, name: str, duration_in_minutes: int, price: float, description: str = None
    ) -> Service:
        new_service = Service(
            name=name,
            description=description,
            duration_in_minutes=duration_in_minutes,
            price=price,
        )
        commit()
        return new_service

    @db_session
    def get_service_by_service_id(self, service_id: int) -> Service:
        service = (
            select(service for service in Service)
            .filter(lambda service: service.service_id == service_id)
            .prefetch(Service.appointments)
        ).get()

        if not service:
            raise ServiceNotFound

        return service

    @db_session
    def list_services(
        self,
        page: int,
        limit: int,
        name: str = None,
        description: str = None,
    ) -> ListingResults[Service]:
        services = select(service for service in Service).prefetch(Service.appointments)

        if name:
            services = services.filter(
                lambda service: name.lower() in service.name.lower()
            )

        if description:
            services = services.filter(
                lambda service: description.lower() in service.description.lower()
            )

        services = services.order_by(Service.service_id)
        results = services.page(page, limit)
        count = services.count()

        return ListingResults(
            results=results[:],
            count=count,
            last_page=int(ceil(count / limit)),
            page=page,
        )

    @db_session
    def update_service_by_service_id(
        self,
        service_id: int,
        name: str = None,
        description: str = None,
        duration_in_minutes: int = None,
        price: float = None,
    ) -> Service:
        service = self.get_service_by_service_id(service_id)
        service.set(
            name=name,
            description=description,
            duration_in_minutes=duration_in_minutes,
            price=price,
        )
        commit()
        return service

    @db_session
    def delete_service_by_service_id(self, service_id: int) -> None:
        service = self.get_service_by_service_id(service_id)
        service.delete()
        commit()


@lru_cache
def repository_factory() -> ServiceRepository:
    return ServiceRepository()
