from fastapi import FastAPI, Request
from fastapi.responses import RedirectResponse
from fastapi.staticfiles import StaticFiles

from tictoc.common.database import database_factory
from tictoc.views.appointments import router as appointments_router
from tictoc.views.authentication import router as authentication_router
from tictoc.views.dashboard import router as dashboard_router
from tictoc.views.exceptions import Unauthorized
from tictoc.views.index import router as index_router
from tictoc.views.services import router as services_router


class TicToc(FastAPI):
    def __init__(self) -> None:
        super().__init__()
        self._initialize()
        self._mount_routers()
        self._mount_handlers()

    def _initialize(self) -> None:
        self.mount("/static", StaticFiles(directory="tictoc/static"), name="static")
        database_factory().start_database()

    def _mount_routers(self) -> None:
        self.include_router(index_router)
        self.include_router(authentication_router)
        self.include_router(dashboard_router)
        self.include_router(appointments_router)
        self.include_router(services_router)

    def _mount_handlers(self) -> None:
        self.add_exception_handler(Unauthorized, self._handle_unauthorized)

    @staticmethod
    def _handle_unauthorized(
        request: Request, exc: Unauthorized  # pylint: disable=unused-argument
    ) -> RedirectResponse:
        return RedirectResponse(
            url=authentication_router.url_path_for("login"), status_code=303
        )
