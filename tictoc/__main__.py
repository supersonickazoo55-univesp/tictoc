import uvicorn

if __name__ == "__main__":
    uvicorn.run("__application__:application", reload=True)
