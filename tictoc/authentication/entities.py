from datetime import datetime

from pony.orm import PrimaryKey, Required

from tictoc.common.database import BaseEntity


class User(BaseEntity):
    _table_ = "users"

    user_id = PrimaryKey(int, auto=True)
    username = Required(str, max_len=20, unique=True)
    password = Required(str, max_len=400)
    email = Required(str, max_len=100, unique=True)
    created_at = Required(datetime, default=datetime.now())
