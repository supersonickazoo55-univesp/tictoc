class UserNotFound(Exception):
    def __init__(self) -> None:
        super().__init__("User not found")


class InvalidPassword(Exception):
    def __init__(self) -> None:
        super().__init__("Password does not match")
