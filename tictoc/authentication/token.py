from datetime import datetime, timedelta
from typing import Any, Dict

from jwt import decode, encode
from jwt.exceptions import DecodeError, ExpiredSignatureError

from tictoc.common.configuration import Configuration, configuration_factory


class TokenManager:

    def __init__(self, configuration: Configuration) -> None:
        self._configuration = configuration

    def create_token(self, user_id: int) -> str:
        payload = {
            "exp": (datetime.now() + timedelta(hours=1)).timestamp(),
            "sub": user_id,
            "iss": "tictoc",
        }
        return encode(
            payload, self._configuration.configuration.authentication.secret, "HS256"
        )

    def decode_token(self, token: str) -> Dict[str, Any]:
        return decode(
            token, self._configuration.configuration.authentication.secret, ["HS256"]
        )

    def is_token_valid(self, token: str) -> bool:
        try:
            self.decode_token(token)
        except (DecodeError, ExpiredSignatureError):
            return False

        return True


def token_factory() -> TokenManager:
    configuration = configuration_factory()
    return TokenManager(configuration)
