from functools import lru_cache
from math import ceil

from pony.orm import commit, db_session, select

from tictoc.common.database.dataclasses import ListingResults

from .entities import User
from .exceptions import UserNotFound


class AuthenticationRepository:

    def __init__(self) -> None:
        pass

    @db_session
    def create_user(self, username: str, email: str, password: str) -> User:
        new_user = User(username=username, email=email, password=password)
        commit()
        return new_user

    @db_session
    def get_user_by_user_id(self, user_id: int) -> User:
        user = User.get(user_id=user_id)

        if not user:
            raise UserNotFound

        return user

    @db_session
    def get_user_by_username(self, username: str) -> User:
        user = User.get(username=username)

        if not user:
            raise UserNotFound

        return user

    @db_session
    def list_users(
        self, page: int, limit: int, username: str = None, email: str = None
    ) -> ListingResults[User]:
        users = select(user for user in User)

        if username:
            users = users.filter(lambda user: username.lower() in user.username.lower())

        if email:
            users = users.filter(lambda user: email.lower() in user.email.lower())

        results = users.page(page, limit)
        count = users.count()

        return ListingResults(
            results=results[:],
            count=count,
            last_page=int(ceil(count / page)),
            page=page,
        )

    @db_session
    def delete_user_by_user_id(self, user_id: int) -> None:
        user = self.get_user_by_user_id(user_id)
        user.delete()
        commit()


@lru_cache
def repository_factory() -> AuthenticationRepository:
    return AuthenticationRepository()
