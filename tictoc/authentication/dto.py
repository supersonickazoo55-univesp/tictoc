from typing import Annotated

from pydantic import constr, field_validator

from tictoc.common.dto import BaseModel
from tictoc.common.exceptions import ValidationError


class LoginRequest(BaseModel):
    username: Annotated[str, constr(max_length=20)]
    password: Annotated[str, constr(max_length=50)]

    @field_validator("username")
    def validate_username(cls, value: str) -> str:  # pylint: disable=no-self-argument
        if " " in value:
            raise ValidationError("Username cannot contain spaces")

        return value.lower()

    @field_validator("password")
    def validate_password(cls, value: str) -> str:  # pylint: disable=no-self-argument
        if " " in value:
            raise ValidationError("Password cannot contain spaces")

        return value
