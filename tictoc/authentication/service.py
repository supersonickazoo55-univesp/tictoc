from functools import lru_cache
from typing import Any, Dict

from argon2 import PasswordHasher
from argon2.exceptions import HashingError, InvalidHashError, VerifyMismatchError

from tictoc.common.configuration import Configuration, configuration_factory

from .exceptions import InvalidPassword, UserNotFound
from .repository import AuthenticationRepository, repository_factory


class AuthenticationService:

    def __init__(
        self, repository: AuthenticationRepository, configuration: Configuration
    ) -> None:
        self._repository = repository
        self._password_hasher = PasswordHasher()
        self._configuration = configuration

    def verify_user(self, username: str, password: str) -> bool:
        user = self._repository.get_user_by_username(username)

        if not user:
            raise UserNotFound

        try:
            self._password_hasher.verify(user.password, password)
        except (VerifyMismatchError, InvalidHashError, HashingError) as exc:
            raise InvalidPassword from exc

        return True

    def create_user(self, username: str, password: str, email: str) -> None:
        hashed_password = self._password_hasher.hash(
            password,
            salt=self._configuration.configuration.authentication.secret.encode(
                "utf-8"
            ),
        )
        self._repository.create_user(
            username=username, password=hashed_password, email=email
        )

    def get_user_by_username(self, username: str) -> Dict[str, Any]:
        return self._repository.get_user_by_username(username)

    def get_user_by_user_id(self, user_id: int) -> Dict[str, Any]:
        return self._repository.get_user_by_user_id(user_id)


@lru_cache
def service_factory() -> AuthenticationService:
    repository = repository_factory()
    configuration = configuration_factory()
    return AuthenticationService(repository, configuration)
