# tictoc

## description
tictoc is an appointment scheduling system made by students of [UNIVESP](https://univesp.br/) using Python. This project was mainly developed to cover the necessities of a hair salon, but modifications can be made to accommodate to other fields.  

## features

## requirements
- python 3.10
- any other python dependency listed in [pyproject.toml](pyproject.toml)

## installation instructions
### Linux
1. clone repository
```shell
git clone git@gitlab.com:supersonickazoo55-univesp/tictoc.git
```

2. change to directory
```shell
cd tictoc
```

3. create a virtual environment  
```shell
python -m venv venv
```

4. open virtual environment  
```shell
source venv/bin/activate
```

5. install application  
```shell
pip install .
```
or for development  
```bash
pip install --editable ".[development]"
```

6. create configuration.toml file (use configuration_example.toml as example)

## configuration
| environment variable | description |
| :-: | :-: |
| TICTOC_CONFIGURATION_FILE | path of configuration file |

## running the application
with virtual environment activated, execute:
```bash
python tictoc
```

